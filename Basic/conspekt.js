/* 

   TypeScript - это статически-типизируемый JS, который в основном служит для создания сложных
и больших приложений.
   Язык TypeScript, который разработала MicroSoft, нужен только на этапе разработки. TypeScript
берет ядро JS и поверх него накладывает некие абстракции, которые относятся к статической типизации,
и позволяют отлавливать ошибки на этапе разработки.
   Устанавливаем пакет typescript:   npm install -g typescript


   Интерфейс - тип, который служит для создания объектов или имплементации классов.  
Мы указываем какие поля, ф-ии и какие вообще элементы должны присутствовать у объектов 
или класов. Интерфейсы ни во что не компилируются, нужны только на этапе разработки


*/



Комманды:

tsc <tsFileName.ts>                    ts file compilate to js file, at same folder and same name 



Пакеты:

typescript                             функционал typescript